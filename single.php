<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();




if ( have_posts() ) : 
    while ( have_posts() ) : the_post();
    
    if (get_the_post_thumbnail_url()){ 
        ?><div class="d-flex container-fluid" style="height:50vh;background:url(<?php echo get_the_post_thumbnail_url(); ?>)  center / cover no-repeat;"></div>
    <?php } else {
        ?><div class="d-flex container-fluid" style="height:10vh;"></div>
    <?php } ?>
    
    <div class="container p-5 bg-light" style="margin-top:-100px">
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <?php 
                
                the_content();
                
                // if( get_theme_mod("enable_sharing_buttons")) picostrap_the_sharing_buttons();
                
                // edit_post_link( __( 'Edit this post', 'picostrap' ), '<p class="text-right">', '</p>' );
                
                // // If comments are open or we have at least one comment, load up the comment template.
                // if (!get_theme_mod("singlepost_disable_comments")) if ( comments_open() || get_comments_number() ) {
                //     comments_template();
                // }
                
                ?>

            </div><!-- /col -->
        </div>
    </div>

<?php
    endwhile;
 else :
     _e( 'Sorry, no posts matched your criteria.', 'picostrap' );
 endif;
 ?>


 

<?php get_footer();

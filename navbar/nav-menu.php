<ul class="navbar-nav nav-fill w-75 mr-auto mb-2 mb-lg-0">
    <!-- no dropdown -->
   <li class="nav-item active">
      <a class="nav-link" aria-current="page" href="#">Home</a>
   </li>
   <!-- with dropdown -->
   <li class="nav-item dropdown has-megamenu">
      <a class="nav-link dropdown-toggle" href="#" id="dropdown1" data-flip="false"
         data-toggle="dropdown" aria-expanded="false">Dropdown1</a>
      <ul class="dropdown-menu megamenu" aria-labelledby="dropdown1">
          <div class="container">
            <div class="row align-self-center d-flex">
                <div class="col-4 col-first-level">
                        <li>
                            <a class="dropdown-item" href="http://www.youtube.com" target="_blank">Action 1 - Youtube</a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#">Drop 1</a>
                        </li>
                </div>
                <div class="col-4 col-second-level">
                        <ul class="">
                        <li><a class="dropdown-item" href="#">Action 1.1</a></li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#">Dropdown1.1.1</a>  
                        </li>
                        </ul>
                </div>
                <div class="col-4 col-third-level">
                        <ul style="display: none">
                            <li><a class="dropdown-item" target="_blank"
                                href="http://www.google.com">Action 1.1.1 - Google</a></li>
                        </ul>
                    <ul class="">
                        <li><a class="dropdown-item" target="_blank"
                            href="http://www.google.com">Action 1.1.1 - Google</a></li>
                    </ul>
                </div>
            </div>
          </div>
      </ul>
   </li>

</ul>